# Quizzz
App for creating simple polls for a group of people.  
User authentication, database and hosting is in firebase.  

## Getting started  
Copy repo  
Install dependencies `npm install`  

### Setting up firebase-project
Log in to firebase `firebase login` (you need to have firebase CLI installed)  
Run `firebase init` and select `hosting`  
Select `Create a new project` and set your public directory to `build`  
Configure as a single-page app (routing will be handled with reach-router)  

Go to [firebase-console](https://console.firebase.google.com/u/0/)  
Open the app you created and add Firebase to your web app  
Enable authentication with email/password and cloud firestore  
Copy this to your firebase rules to let only logged in users access your database  
```
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if request.auth.uid != null;
    }
  }
}
```  
Get `firebaseConfig` from project settings and copy them into `src/firebaseConfig.js`. Export the object.  

## Development  
`npm run develop` will start webpack-dev-server

## Production
`npm run deploy` - Creates production build and deploys to firebase