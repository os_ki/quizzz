import React, { Component } from "react"

// create context for global state
const AppContext = React.createContext(null)

// create AppProvider
class AppProvider extends Component {
  constructor() {
    super()
    // enable components to read and write to user
    this.state = {
      user: false,
      setUser: user => {
        this.setState({ user })
      },
      error: false,
      setError: error => {
        this.setState({ error })
      },
    }
  }
  render() {
    const { children } = this.props
    return (
      <AppContext.Provider value={this.state}>{children}</AppContext.Provider>
    )
  }
}

// export the context for other components to use
export default AppContext

// export the contextProvider for root component to wrap the app inside it
export { AppProvider }
