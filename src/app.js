import React, { useEffect, useContext } from 'react'
import { render } from 'react-dom'
import { navigate } from "@reach/router"
import firebase from '@firebase/app'
import '@firebase/auth'
import { AppProvider } from "./context/app"
import AppContext from "./context/app"
import firebaseConfig from "./firebaseConfig"
import Main from './components/main'

const App = () => {

  // get user, setUser, error and setError from context
  const { setUser } = useContext(AppContext)

  useEffect(() => {
    // initialize firebase
    firebase.initializeApp(firebaseConfig)

    // listen on login and logout to display right views
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        setUser(user)
      } else {
        // user has logged out
        setUser(false)
        navigate('login')
      }
    })
  }, [])

  return <Main />
}



render(
  <AppProvider>
    <App />
  </AppProvider>,
  document.getElementById('target')
)
