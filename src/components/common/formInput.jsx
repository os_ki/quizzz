import React from 'react'
import './formInput.scss'
/**
 * TextInput
 *
 * @param {string} label - Label
 * @param {string} placeholder - Placeholder text
 * @param {function} onChange - handles onChange event
 * @param {string} autoComplete - autoComplete
 * @param {string} name - name
 * @param {string} type - input-type
 */

const FormInput = ({
  label,
  placeholder = '',
  onChange,
  autoComplete = 'off',
  name = '',
  type,
  value
}) => (
    <div className='formInput'>
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        name={name}
        autoComplete={autoComplete}
        aria-label={label}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
      />
    </div>
  )

export { FormInput }