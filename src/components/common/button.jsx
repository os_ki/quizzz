import React from 'react'
import { Spinner } from './'
import './button.scss'

/**
 * 
 * @param {Function} onClick
 * @param {string} type - htmlType of the button
 * @param {Boolean} loading 
 */
const Button = ({ onClick, type = 'button', loading = false, children }) => {

  const handleClick = () => {
    !loading && type !== 'submit' && onClick()
  }

  return (
    <button
      onClick={handleClick}
      className='button button--border'
      type={type}
    >
      {children}
      {loading && <Spinner size='20px' />}
    </button>
  )
}

export { Button }