import React, { useState } from 'react'
import { FormInput, Button } from '../common';

/**
 * TextInput
 *
 * @param {string} label - Label
 * @param {string} placeholder - Placeholder text
 * @param {function} onChange - handles onChange event
 * @param {string} autoComplete - autoComplete
 * @param {string} name - name
 * @param {string} type - input-type
 */

const FieldForm = ({ addNewField }) => {
  const [title, setTitle] = useState('')
  const [info, setInfo] = useState('')
  const [errors, setErrors] = useState({})

  const handleSubmit = () => {
    // validate fields
    validate().then(() => {
      addNewField({ title, info })
      setTitle('')
      setInfo('')
    }).catch((err) => {
      console.error('validation failed', err)

    })
  }

  const validate = () => (
    new Promise((resolve, reject) => {
      // gather errors in an object
      let obj = {}
      // title is required
      if (!title.length) {
        obj.title = 'Title is required'
      }
      setErrors(obj)
      // if there is errors, reject promise
      Object.keys(obj).length < 1 ? resolve() : reject()
    })
  )



  return (
    <>
      <div>
        <h3>New field</h3>
        <FormInput
          type='text'
          placeholder='Field title'
          addonBefore='Field title'
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          style={{ marginBottom: '0.5rem' }}
          label='Field title'
        />
      </div>
      <div>
        <FormInput
          type='text'
          placeholder='Field info'
          addonBefore='Field info'
          value={info}
          onChange={(e) => setInfo(e.target.value)}
          style={{ marginBottom: '0.5rem' }}
          label='Field info'
        />
      </div>
      <Button onClick={handleSubmit} type='button'>Add Field</Button>
    </>
  )
}

export default FieldForm