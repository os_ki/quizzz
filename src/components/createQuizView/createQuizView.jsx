import React, { useState } from 'react'
import { navigate } from '@reach/router'
import { createQuiz } from '../../services'
import FieldForm from './fieldForm';
import { FormInput, Button } from '../common';
import './createQuizView.scss'

const CreateQuizView = () => {
  const [loading, setLoading] = useState(false) // true | false
  const [type, setType] = useState('Radio')
  const [fields, setFields] = useState([])
  const [title, setTitle] = useState('')
  const [info, setInfo] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault();
    if (loading) return

    // TODO: handle validations
    if (fields.length < 1) return

    setLoading(true)
    const data = {
      type,
      fields,
      title,
      info
    }
    // write quiz to database
    createQuiz(data)
      .then((quizId) => {
        navigate(`/quizzes/${quizId}`)
      })
      .catch((err) => {
        // TODO: handle errors
        console.error("error while creating writing quiz to database", err)
      }).then(() => setLoading(false))
  }

  const addNewField = (newField) => {
    const arr = [...fields, newField]
    setFields(arr)
  }

  const removeField = (index) => {
    let arr = [...fields]
    arr.splice(index, 1)
    setFields(arr)
  }

  return (
    <main className='createQuizView'>
      <div>
        <h1>Create Quiz</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <h3>Title</h3>
            <FormInput
              type='text'
              placeholder='title'
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>

          <div>
            <h3>Info</h3>
            <FormInput
              type='text'
              placeholder='info'
              value={info}
              onChange={(e) => setInfo(e.target.value)}
            />
          </div>

          <div>
            <h3>Quiz type</h3>
            <select value={type} onChange={(e) => setType(e.target.value)}>
              <option value="Radio">Radio</option>
              <option disabled value="Checkbox">Checkbox</option>
              <option disabled value="Textfields">Textfields</option>
            </select>
          </div>

          <div className='fieldSection'>
            <FieldForm addNewField={addNewField} />
            <h3>Fields</h3>
            <ul>
              {
                fields.map((field, index) => (
                  <li key={field.title}>
                    <div className='fieldDataGroup'>
                      <div><h4>Title:</h4><span>{field.title}</span></div>
                      <div><h4>Info:</h4><span>{field.info}</span></div>
                    </div>
                    <Button type='button' onClick={() => removeField(index)} style={{ marginLeft: 'auto' }}>x</Button>
                  </li>
                ))
              }
            </ul>

          </div>

          <div>
            <Button type='submit' loading={loading} >Submit</Button>
          </div>

        </form>
      </div>

      <div style={{ width: '1000px', maxWidth: '95%', margin: '2rem auto', backgroundColor: '#fff' }}>
        <h2>Preview of your quiz</h2>
        {`
        {
          title: "${title}",
          type: "${type}",
          fields: [
            ${fields.map(i => `
              {
                title: "${i.title}"
                ${i.info && `, info: "${i.info}"`}
              }
            `)}
          ]
        }
      `}
      </div>
    </main>
  )
}


export default CreateQuizView