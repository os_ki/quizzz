import React, { useEffect, useState } from 'react'
import { getQuizzes } from '../../services/database'
import { Spinner } from '../common'
import Quiz from './quiz'
import './quizzes.scss'

const Quizzes = () => {
  const [data, setData] = useState([])
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getQuizzes()
      .then((data) => {
        setData(data)
      })
      .catch((err) => {
        console.error(err)
        setError(err.code)
      })
      .then(() => setLoading(false))
  }, [])

  if (loading) return <Spinner />

  return (
    <div className='quizzes'>
      <h1>Quizzes</h1>
      {
        data.map((quiz) => {
          return <Quiz key={quiz.id} {...quiz} />
        })
      }
    </div>
  )
}

export default Quizzes