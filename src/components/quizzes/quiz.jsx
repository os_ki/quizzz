import React, { useState, useEffect } from 'react'
import { Link } from '@reach/router'
import {
  ArcSeries, Tooltip,
  ChartProvider, Circle
} from 'rough-charts'
import './quiz.scss'

const Quiz = ({ fields, info, title, id, type, answers }) => {
  const [graphData, setGraphData] = useState(false)

  useEffect(() => {
    // convert radio-quiz -data for rough-charts
    if (answers && fields && type === 'Radio') {
      const data = fields.map((field) => ({ name: field.title, value1: 0 }))
      const answerArray = Object.values(answers)
      data.forEach(elem => {
        elem.value1 = answerArray.filter(i => i === elem.name).length
      })
      setGraphData(data)
    }
  }, [])


  return (
    <div className='quiz'>
      <div>
        <h2><Link to={`/quizzes/${id}`}>{title}</Link></h2>
        <h3>{info}</h3>
      </div>
      <div>
        {
          fields.map((field, index) => (
            <h4 key={field.title}>{index}: {field.title}</h4>
          ))
        }
      </div>
      <div>
        {graphData && <ChartProvider
          height={400}
          data={graphData}
          margin={{ top: 30, left: 0 }}
        >
          <ArcSeries
            dataKey="value1"
            options={{
              fill: 'red',
              stroke: "#ffffff50",
              strokeWidth: 1,
              roughness: 2,
              fillStyle: 'hachure'
            }}
          />
          <Tooltip width={150} height={70} />
        </ChartProvider>}
      </div>
    </div>
  )
}

const renderTooltip = (activeItem, number) => {
  const width = 150
  const height = 70
  const {
    name, value1
  } = activeItem

  return (
    <React.Fragment>
      <text
        x={width / 2}
        y={20}
        textAnchor="middle"
        stroke="black"
        fill="black"
      >
        {name}
      </text>
      <text
        x={width / 2}
        y={40}
        textAnchor="middle"
        stroke="black"
        fill="black"
      >
        {value1}
      </text>
    </React.Fragment>
  )
}


export default Quiz