import React, { useContext } from 'react'
import { Router, Link } from "@reach/router"
import AppContext from "../context/app"
import Quizzes from './quizzes/quizzes'
import CreateQuizView from './createQuizView/createQuizView'
import ResultView from './resultView/resultView'
import Login from './login'
import QuizPage from './quizView/quizPage'

const AppRouter = () => {
  const { user } = useContext(AppContext)
  return (
    user ? (
      <Router>
        <Quizzes path="/" />
        <QuizPage path='quizzes/:id' />
        <CreateQuizView path="create-quiz" />
        <ResultView path="results/:id" />
      </Router>
    ) : (
        <Login />
      )
  )
}

export default AppRouter