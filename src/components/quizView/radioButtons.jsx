import React, { useState } from 'react'
import { Button } from '../common'
import './radioButtons.scss'

/**
 * RadioButtons
 * @param {Array} fields - quiz-fields
 * @param {string} fields.title
 * @param {string} fields.info
 * @param {Function} submit - submit form
 */

const RadioButtons = ({ fields, submit }) => {
  const [radioButtons, setRadioButtons] = useState(() => fields)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState()

  const handleSubmit = (e) => {
    e.preventDefault()
    if (loading) return
    setLoading(true)
    // validate fields
    validate().then((selected) => {
      submit(selected)
    }).catch((err) => {
      console.error('validation failed', err)
      setError(err)
    }).then(() => setLoading(false))
  }

  const validate = () => (
    new Promise((resolve, reject) => {
      // One must be selected
      const selected = radioButtons.find((elem) => elem.checked)
      // if there is errors, reject promise
      selected ? resolve(selected.title) : reject('None was selected')
    })
  )

  const handleChange = (index, e) => {
    let arr = radioButtons.map((item) => ({ ...item, checked: false }))
    arr[index] = { ...arr[index], checked: e.target.checked }
    setRadioButtons(arr)
  }


  return (
    <form onSubmit={handleSubmit} className='radioButtons'>
      {fields.map((field, index) => {
        return (
          <div key={field.title}>
            <input
              id={field.title}
              type='radio'
              onChange={(e) => handleChange(index, e)} checked={Boolean(radioButtons[index].checked)}
            />
            <label htmlFor={field.title}>
              {field.title}
            </label>
          </div>
        )
      })}
      <Button type='submit' loading={loading}>Submit</Button>
      {error && <p>{error}</p>}
    </form>)
}

export default RadioButtons