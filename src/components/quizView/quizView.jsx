import React, { useState } from 'react'
import { submitAnswer } from '../../services'
import QuizForm from './quizForm'
import './quizView.scss'
/**
 * QuizView
 * @param {Object} data - quiz-data
 * @param {string} data.title 
 * @param {string} data.info
 * @param {string} data.type - 'Radio' || 'Checkbox' || 'Textfield'
 * @param {Array} data.fields - fields of quiz
 * @param {string} data.fields.title
 * @param {string} data.fields.info
 * @param {string} id - id of the quiz
 */

const QuizView = ({ data, id }) => {
  const [submitted, setSubmitted] = useState(false)

  const handleSubmit = (value) => {
    submitAnswer(value, id).then(() => {
      setSubmitted(true)
    }).catch((err) => {
      console.error(err)
    })
  }

  if (submitted) {
    return <div className='quizView'>submitted</div>
  }
  return (
    <div className='quizView'>
      <h1>{data.title}</h1>
      <p>{data.info}</p>
      <QuizForm type={data.type} fields={data.fields} submit={handleSubmit} />
    </div>
  )
}

export default QuizView