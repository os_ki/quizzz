import React, { useState, useEffect, useContext } from 'react'
import { getQuiz } from '../../services'
import QuizView from './quizView'
import { Spinner } from '../common'
import AppContext from '../../context/app'

/**
 * QuizPage - handles getting data for specific quiz. Quiz id is passed by AppRouter
 *
 * @param {string} id - firestore id for quiz
 */

const QuizPage = ({ id }) => {
  const [data, setData] = useState(false)
  const [loading, setLoading] = useState(true)
  const { user } = useContext(AppContext)

  useEffect(() => {
    getQuiz(id)
      .then((data) => {
        if (hasUserAnswered(data.answers)) {
          // TODO: maybe inform user some other way
          alert('You have already answered this quiz. Your answer will override your previous answer.')
        }
        setData(data)
      })
      .catch((err) => {
        console.error(err)
      })
      .then(() => {
        setLoading(false)
      })
  }, [id])

  // check from answers-object's keys if user have already answered the quiz
  const hasUserAnswered = (answers) => {
    return answers && Object.keys(answers).some(key => key === user.uid)
  }

  if (loading) return <Spinner />
  if (data) return <QuizView data={data} id={id} />
  return <div>Quiz not found :(</div>

}

export default QuizPage