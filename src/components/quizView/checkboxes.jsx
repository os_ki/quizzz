import React, { useState } from 'react'
import { Button } from '../common'
import './checkboxes.scss'
/**
 * checkboxes
 * @param {Array} fields - quiz-fields
 * @param {string} fields.title
 * @param {string} fields.info
 * @param {Function} submit - submit form
 */
const Checkboxes = ({ fields, submit }) => {
  const [checkboxes, setCheckboxes] = useState(() => fields)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState()

  const handleSubmit = (e) => {
    e.preventDefault()
    if (loading) return
    setLoading(true)
    // validate fields
    validate().then((selected) => {
      submit(selected)
    }).catch((err) => {
      console.error(err)
    }).then(() => setLoading(false))
  }

  const validate = () => (
    new Promise((resolve, reject) => {
      // find all checked values and return array of checked titles
      const selected = checkboxes.reduce((accumulator, elem) => {
        if (elem.checked) accumulator.push(elem.title)
        return accumulator
      }, [])
      // if there is errors, reject promise
      selected ? resolve(selected) : reject('Unknown error')
    })
  )

  const handleChange = (index, e) => {
    let arr = [...checkboxes]
    arr[index] = { ...arr[index], checked: e.target.checked }
    setCheckboxes(arr)
  }

  return (
    <form onSubmit={handleSubmit} className='checkboxes'>
      {fields.map((field, index) => {
        return (
          <div key={field.title}>
            <label>{field.title}</label>
            <input type='checkbox' onChange={(e) => handleChange(index, e)} checked={checkboxes[index].checked} />
          </div>
        )
      })}
      <Button type='submit' loading={loading}>Submit</Button>
      {error && <p>{error}</p>}
    </form>
  )
}

export default Checkboxes