import React, { useState } from 'react'
import { FormInput, Button } from '../common'

const TextFields = ({ data, submit }) => {
  const [textareas, setTextareas] = useState(() => data)
  const { TextArea } = Input
  const handleChange = (index, e) => {
    let arr = [...textareas]
    arr[index] = { ...arr[index], value: e.target.value }
    setTextareas(arr)
  }

  const handleSubmit = () => {
    submit()
  }

  return (<form onSubmit={handleSubmit}>
    {data.map((field, index) => {
      return (
        <div key={field.title}>
          <h3>{field.title}</h3>
          <FormInput onChange={(e) => handleChange(index, e)} />
        </div>
      )
    })}
    <div>
      <Button type='submit'>Submit</Button>

    </div>
  </form>)
}

export default TextFields