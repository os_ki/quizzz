## quizView -folder
Presents the view for single quiz

`quizPage` - Handles getting data from database by quiz id (which is passed in url). Renders loading while fetching data and 404 when data can't be found.  

`quizView` - presents quiz with given data.  

### Form types  
Different components for different types of quezzes:  
1. 'radioButtons' for... well radio-buttons  
TODO: Let's make atleast checkbox and textfields  
