import React from 'react'
import Checkboxes from './checkboxes'
import RadioButtons from './radioButtons'
import TextFields from './textFields'

const QuizForm = ({ type, ...props }) => {
  switch (type) {
    case 'Checkbox':
      return <Checkboxes {...props} />
    case 'Radio':
      return <RadioButtons {...props} />
    case 'Textfield':
      return <TextFields {...props} />
    default:
      return <div>Invalid</div>
  }
}

export default QuizForm