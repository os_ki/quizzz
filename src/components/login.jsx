import React, { useState, useEffect, useContext } from "react"
import firebase from "@firebase/app"
import "@firebase/auth"
import { loginErrors, generalErrors } from "../data/microcopy"
import AppContext from "../context/app"
import { FormInput, Card, Button } from "./common"
import './login.scss'

const Login = () => {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [loading, setLoading] = useState(false)
  const { error, setError } = useContext(AppContext)

  const onSubmit = e => {
    e.preventDefault()
    if (loading) return
    setLoading(true)

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        setLoading(false)
        setError(false)
      })
      .catch(err => {
        console.warn("error signingIn", err)
        setError(loginErrors[err.code] || generalErrors.unexpectedError)
      })
      .then(() => {
        setLoading(false)
      })
  }

  return (
    <Card>
      <form onSubmit={onSubmit} className='login'>
        <h2>Login</h2>
        <FormInput
          type="text"
          name="email"
          id="email"
          autoComplete="username"
          aria-label="Email"
          placeholder="Email"
          onChange={e => setEmail(e.target.value)}
        />
        <FormInput
          type="password"
          name="password"
          id="password"
          autoComplete="current-password"
          aria-label="Password"
          placeholder="Password"
          onChange={e => setPassword(e.target.value)}
        />
        <div className="buttons">
          <Button type="submit" loading={loading}>
            Kirjaudu sisään
          </Button>
        </div>
        {error && <p className="error">{error}</p>}
      </form>
    </Card>
  )
}

export default Login
