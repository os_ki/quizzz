import React, { useContext } from 'react'
import { Link } from "@reach/router"
import { logout } from '../../services'

const Naviation = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link to='/'>Quizzes</Link>
        </li>
        <li>
          <Link to='create-quiz'>Create quiz</Link>
        </li>
        <li>
          <Link to='/login' onClick={logout}>Logout</Link>
        </li>
      </ul>
    </nav>
  )
}

export default Naviation