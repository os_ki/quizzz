import React, { useContext } from 'react'
import AppContext from "../context/app"
import AppRouter from './appRouter'
import Naviation from './navigation/navigation'
import '../styles/main.scss'

const Router = () => {
  const { user } = useContext(AppContext)

  return (
    <main>
      <AppRouter />
      {user && <Naviation />}
    </main>
  )
}

export default Router