import firebase from '@firebase/app'
import '@firebase/firestore'
import '@firebase/auth'


/**
 * Write quiz to firestore
 * @param {Object} data - data of the quiz
 * @returns {Promise} Promise object
 */
const createQuiz = (data) => (
  new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('quizzez')
      .add({
        ...data,
        creator: firebase.auth().currentUser.uid
      })
      .then((docRef) => {
        resolve(docRef.id)
      })
      .catch((err) => {
        reject(err)
      })
  })
)

/**
 * Add new answer to database into quizzez-collection. 
 * @param {string} value - value to be submitted
 * @param {string} id - id of the quiz
 * @returns {Promise} - Promise object
 */
const submitAnswer = (value, id) => (
  new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('quizzez')
      .doc(id)
      .update({ [`answers.${firebase.auth().currentUser.uid}`]: value })
      .then(() => {
        resolve()
      })
      .catch((err) => {
        reject(err)
      })
  })
)

/**
 * Get single quiz from firestore by its id
 * @param {string} id - id of the quiz
 * @returns {Promise} Promise object
 */
const getQuiz = (id) => (
  new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('quizzez')
      .doc(id)
      .get()
      .then((doc) => {
        doc.exists ? resolve(doc.data()) : reject({ code: 'Document not found' })
      })
      .catch((err) => {
        reject(err)
      })
  })
)

/**
 * Get all quizzes
 * @returns {Promise} Promise object
 */
const getQuizzes = () => {
  const data = []
  return new Promise((resolve, reject) => {
    firebase
      .firestore()
      .collection('quizzez')
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          data.push({ ...doc.data(), id: doc.id })
        })
        resolve(data)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export { createQuiz, getQuiz, submitAnswer, getQuizzes }