import firebase from '@firebase/app'
import '@firebase/auth'


const logout = () => {
  firebase.auth().signOut()
}

export { logout }