const passwordChangeErrors = {
  "auth/wrong-password": "Nykyinen salasanasi ei mennyt oikein",
  "auth/weak-password": "Uuden salasana tulee olla vähintään 6 merkkiä pitkä.",
  "password-reset-unknown":
    "Salasanan vaihtamisessa tapahtui odottamaton virhe. Yritä myöhemmin uudestaan.",
  verifyPasswordMismatch:
    "Uusi salasana ja vahvista uusi salasana -kentät eivät täsmää.",
}

const orderNewPasswordErrors = {
  "auth/invalid-email": "Sähköposti on huonosti muotoiltu.",
  "auth/user-not-found": "Sähköpostiosoitteelle ei löytynyt aktiivista tiliä.",
}

const loginErrors = {
  "auth/wrong-password": "Väärä käyttäjätunnus tai salasana.",
  "auth/user-not-found": "Väärä käyttäjätunnus tai salasana.",
  "auth/invalid-email": "Väärä käyttäjätunnus tai salasana.",
  "auth/too-many-requests":
    "Liian monta virheellistä kirjautumisyritystä. Yritä myöhemmin uudestaan.",
}

const generalErrors = {
  unexpectedError: "Tapahtui odottamaton virhe. Yritä myöhemmin uudestaan.",
  userFetchError: "Virhe haettaessa käyttäjän tietoja.",
}

export {
  passwordChangeErrors,
  loginErrors,
  generalErrors,
  orderNewPasswordErrors,
}
